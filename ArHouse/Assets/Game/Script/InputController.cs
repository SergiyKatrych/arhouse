﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputController : MonoBehaviour
{
	private HouseController _house;
	private bool isLongTapWasBefore = false;
	private bool isTwoTouchesWasBefore = false;

	private float movementSlowDownKoef = 25f;
	private float rotatingScalingSlowDownKoef = 25f;

	#region Touch
	// Subscribe to events
	void OnEnable()
	{
		_house = GetComponent<HouseController>();

		EasyTouch.On_TouchStart2Fingers += OnTouchStart2Fingers;
		EasyTouch.On_TouchUp += OnTouchUp;
		EasyTouch.On_TouchUp2Fingers += OnTouchUp2Fingers;
	
		EasyTouch.On_Swipe += OnSwipe;
		EasyTouch.On_SwipeEnd += OnSwipeEnd;

		EasyTouch.On_LongTapStart += OnLongTapStart;
		EasyTouch.On_LongTap += OnLongTap;

		EasyTouch.On_Twist += OnTwist;
		EasyTouch.On_Pinch += OnPinch;
	}


	void OnDisable()
	{
		UnsubscribeEvent();
	}

	void OnDestroy()
	{
		UnsubscribeEvent();
	}

	public void ResetTouchBools()
	{
		isLongTapWasBefore = false;
		isTwoTouchesWasBefore = false;
		operatedTouchUps = 0;
	}

	// Unsubscribe to events
	void UnsubscribeEvent()
	{
		EasyTouch.On_TouchStart2Fingers -= OnTouchStart2Fingers;
		EasyTouch.On_TouchUp -= OnTouchUp;
		EasyTouch.On_TouchUp2Fingers += OnTouchUp2Fingers;

		EasyTouch.On_Swipe -= OnSwipe;
		EasyTouch.On_SwipeEnd -= OnSwipeEnd;

		EasyTouch.On_LongTapStart -= OnLongTapStart;
		EasyTouch.On_LongTap -= OnLongTap;


		EasyTouch.On_Twist -= OnTwist;
		EasyTouch.On_Pinch -= OnPinch;
	}


	void OnTouchStart2Fingers(Gesture gesture)
	{
		if (!isLongTapWasBefore)
		{
			isTwoTouchesWasBefore = true;
		}
	}


	private int operatedTouchUps = 0;

	void OnTouchUp(Gesture gesture)
	{		
		if (EasyTouch.GetTouchCount() == 2 && operatedTouchUps > 0)
		{
			if (--operatedTouchUps == 0)
			{
				isTwoTouchesWasBefore = false;
			}
		}
		else if (EasyTouch.GetTouchCount() == 1)
		{
			if (isLongTapWasBefore)
			{
				_house.OnMovementEnded();
				isLongTapWasBefore = false;
			}
		}
	}


	void OnTouchUp2Fingers(Gesture gesture)
	{
		operatedTouchUps = 2;
	}

	#region One Touch

	public void OnSwipe(Gesture gesture)
	{
		if (isLongTapWasBefore && !isTwoTouchesWasBefore)
		{
			_house.IsEnableHouseToTap = false;
			transform.localPosition = 
				transform.localPosition +
			new Vector3(gesture.deltaPosition.x * Time.deltaTime / movementSlowDownKoef, 0, gesture.deltaPosition.y * Time.deltaTime / movementSlowDownKoef);
		}
	}

	public void OnSwipeEnd(Gesture gesture)
	{
		if (!isLongTapWasBefore && !isTwoTouchesWasBefore)
		{
			_house.IsEnableHouseToTap = false;
			if (gesture.swipeVector.y > 0)
			{
				OnSwipeUp();
			}
			else
			{
				OnSwipeDown();
			}
		}
	}

	private void OnSwipeUp()
	{
		_house.AddAdditionalFloor();
	}

	private void OnSwipeDown()
	{

		_house.RemoveAdditionalFloor();
	}

	#region Move
	private Color houseDefaultColor;

	void OnLongTapStart(Gesture gesture)
	{
		if (!isTwoTouchesWasBefore)
		{
			_house.OnMovementBegan();
			isLongTapWasBefore = true;
			_house.IsEnableHouseToTap = false;
		}
	}

	void OnLongTap(Gesture gesture)
	{
		if (!isTwoTouchesWasBefore)
		{
			transform.localPosition = 
				transform.localPosition +
			new Vector3(gesture.deltaPosition.x * Time.deltaTime / movementSlowDownKoef, 0, gesture.deltaPosition.y * Time.deltaTime / movementSlowDownKoef);
		}
	}

	#endregion
	#endregion

	#region Two touches
	void OnTwist(Gesture gesture)
	{
		if (!isLongTapWasBefore)
		{
			transform.Rotate(new Vector3(0, -gesture.twistAngle / 2f, 0));
			_house.IsEnableHouseToTap = false;
		}
	}

	private float targetZoom = 0;

	private void OnPinch(Gesture gesture)
	{
		if (!isLongTapWasBefore)
		{
			//it's too fast, so we need to slow scaling down.
			float zoom = Time.deltaTime * gesture.deltaPinch / 3;

			Vector3 scale = transform.localScale;
			_house.ScaleOn(new Vector3(scale.x + zoom / rotatingScalingSlowDownKoef, scale.y + zoom / rotatingScalingSlowDownKoef, scale.z + zoom / rotatingScalingSlowDownKoef));
			_house.IsEnableHouseToTap = false;
		}
	}
	#endregion
	#endregion
}
