﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopFloorController : FloorScript
{


	[SerializeField] private Canvas _popup;
	private RectTransform _popupPanel;

	[SerializeField] private string targetSiteUrl = "www.tsukat.com";
	private Camera userCamera;

	internal void InitFloor(Camera userARCamera)
	{
		_popupPanel = _popup.transform.GetChild(0).GetComponent<RectTransform>();
		if (userARCamera != null)
		{
			userCamera = userARCamera;
		}
	}

	private void Update()
	{
		if (_popup.gameObject.activeSelf)
		{
			var targetPos = GetScreenPosition(gameObject.transform);

			_popupPanel.anchoredPosition = targetPos;
		}
	}


	public void ShowPopup()
	{

		if (_popup.gameObject.activeSelf)
		{
			_popup.gameObject.SetActive(false);
		}
		else
		{
			_popup.renderMode = RenderMode.ScreenSpaceOverlay;
			_popup.worldCamera = userCamera;
			var targetPos = GetScreenPosition(transform);
			_popupPanel.anchoredPosition = targetPos;
			_popup.gameObject.SetActive(true);
		}
	}

	public  Vector3 GetScreenPosition(Transform objTransform)
	{
		Vector3 pos;

		float x = userCamera.WorldToScreenPoint(objTransform.position).x - Screen.width * 0.25f;
		float y = userCamera.WorldToScreenPoint(objTransform.position).y - Screen.height * 0.5f;
		pos = new Vector3(x, y);    
		return pos;    
	}

	public void OpenWWWPage()
	{
		Debug.Log("try to open " + targetSiteUrl);
		Application.OpenURL(targetSiteUrl);
		Debug.Log("opened " + targetSiteUrl);
	}
}
