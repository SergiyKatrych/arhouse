﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorScript : MonoBehaviour
{

	[SerializeField] private Transform _nextFloorBottom;

	public Transform NextFloorBottom
	{
		get { return _nextFloorBottom; }
	}

	private Color defaulFloorColor;
	[SerializeField] private Renderer _renderer;
	private Material _floorMaterail;

	public Material FloorMaterial
	{
		get
		{
			if (_floorMaterail == null)
			{
				_floorMaterail = _renderer.material;
			}
			
			return _floorMaterail;
		}
	}
	// Use this for initialization
	void Awake()
	{
		_floorMaterail = _renderer.material;
		defaulFloorColor = FloorMaterial.color;
			
		if (_nextFloorBottom == null)
		{
			if (transform.childCount > 0)
			{
				_nextFloorBottom = transform.GetChild(0);
			}
		}
	}

	public float GetHeight()
	{
		return _renderer.bounds.size.y;
	}

	public void ResetMaterialColor()
	{
		FloorMaterial.color = defaulFloorColor;
	}
}
