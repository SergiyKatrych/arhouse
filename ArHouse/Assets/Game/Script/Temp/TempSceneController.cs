﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GoogleARCore.HelloAR;

public class TempSceneController : MonoBehaviour
{

	public HouseController _housePrefab;
	private HouseController _currentHouseObject;


	private void CreateHouse(Vector3 targetPos)
	{
		// Intanstiate an Andy Android object as a child of the anchor; it's transform will now benefit
		// from the anchor's tracking.
		_currentHouseObject = Instantiate(_housePrefab, targetPos, Quaternion.identity);
		_currentHouseObject.GetComponent<PlaneAttachment>().enabled = false;
		// Andy should look at the camera but still be flush with the plane.
		_currentHouseObject.transform.LookAt(Camera.main.transform);
		_currentHouseObject.transform.rotation = Quaternion.Euler(0.0f,
			_currentHouseObject.transform.rotation.eulerAngles.y, _currentHouseObject.transform.rotation.z);
		
		_currentHouseObject.InitHouse(Camera.main);

	}

#region Interaction

	public void CreateHouse()
	{
		if (_currentHouseObject == null)
		{
			CreateHouse(Vector3.zero);
		}
	}

	public void DestroyHouse()
	{
		if (_currentHouseObject != null)
		{
			Destroy(_currentHouseObject.gameObject);
			_currentHouseObject = null;
		}
	}

	public void AddFloor()
	{
		_currentHouseObject.AddAdditionalFloor();
	}

	public void RemoveFloor()
	{
		_currentHouseObject.RemoveAdditionalFloor();
	}

	public void AreaRedPressed()
	{
		Debug.Log("AreaRedPressed");
		_currentHouseObject.OnTapOTop();
	}

	public void AreaGreenPressed()
	{
		Debug.Log("AreaGreenPressed");

		_currentHouseObject.ChangeLightOnEntrance();
	}

	public void AreaBluePressed()
	{
		Debug.Log("AreaBluePressed");
		_currentHouseObject.OnTapOnBottom();
	}
	#endregion

	#region ToolTips
	public void ToAR()
	{
		SceneManager.LoadScene("Gameplay");
	}
#endregion
}
